USE [SWP]
GO

INSERT INTO [dbo].[mentee_to_mentor_request]
           ([mentee_id]
           ,[bio]
           ,[status]
           ,[created])
VALUES
			(1, 'Mentee Bio 1', 'Pending', GETDATE()),
           (2, 'Mentee Bio 2', 'Approved', GETDATE()),
           (3, 'Mentee Bio 3', 'Rejected', GETDATE()),
           (4, 'Mentee Bio 4', 'Pending', GETDATE());

